from sqlalchemy import and_, func, insert, select, update, delete
from app.coins.dao import CoinExchangeDAO, CoinsDAO
from app.dao.base import BaseDAO
from app.wallets.models import Wallet_amount, Wallets
from app.database import async_session_maker
from app.database import engine
from app.wallets.schema import SchemaAddAmount, SchemaAddWallet, SchemaCheckCoin


class WalletsDAO(BaseDAO):
    model = Wallets

    @classmethod
    async def add_wallet(cls, new_wallet: SchemaAddWallet):
        async with async_session_maker() as session:
            query = select(Wallets).filter_by(owner_id=new_wallet.owner_id)
            result = await session.execute(query)
            wallet = result.scalar()
            if wallet is None:
                add_wallet = insert(Wallets).values(
                    owner_id=new_wallet.owner_id, coin_id=new_wallet.coin_id, bot_id=new_wallet.bot_id
                )
                new_wallet = await session.execute(add_wallet)
                await session.commit()
                return new_wallet
            elif (
                    (new_wallet.coin_id == wallet.coin_id)
                    and (new_wallet.owner_id == wallet.owner_id)
                    and (new_wallet.bot_id == wallet.bot_id)
            ):
                return "error"
            else:
                add_wallet = insert(Wallets).values(
                    owner_id=new_wallet.owner_id, coin_id=new_wallet.coin_id, bot_id=new_wallet.bot_id
                )
                new_wallet = await session.execute(add_wallet)
                await session.commit()
                return new_wallet

    @classmethod
    async def check_coin(cls, check_coin: SchemaCheckCoin):
        async with async_session_maker() as session:
            query = select(Wallets).filter(Wallets.owner_id == check_coin.owner_id,
                                           Wallets.coin_id == check_coin.coin_id,
                                           Wallets.bot_id == check_coin.bot_id
                                           )
            result = await session.execute(query)
            wallets = result.mappings().all()
            if not wallets:
                return 0
            else:
                return 1

    @classmethod
    async def view_wallet_id(cls, id: int):
        async with async_session_maker() as session:
            all_wallet = []
            query = select(Wallets).filter_by(owner_id=id)
            result = await session.execute(query)
            wallets = result.mappings().all()
            for wallet in range(len(wallets)):
                wallet_dict = wallets[wallet]["Wallets"].__dict__
                all_wallet.append(wallet_dict)
            return all_wallet

    @classmethod
    async def view_last_wallet(cls):
        async with async_session_maker() as session:
            query = select(Wallets).order_by(Wallets.id.desc()).limit(1)
            result = await session.execute(query)
            last_wallet = result.scalar()

            if last_wallet:
                return last_wallet.id
            else:
                return None

    @classmethod
    async def view_wallet_amount_id(cls, check_wallet: SchemaCheckCoin):
        async with async_session_maker() as session:
            query = select(Wallets).filter(Wallets.owner_id == check_wallet.owner_id,
                                           Wallets.coin_id == check_wallet.coin_id,
                                           Wallets.bot_id == check_wallet.bot_id
                                           )
            result = await session.execute(query)
            wallets = result.mappings().all()
            if not wallets:
                return "Этой монеты нет"
            else:
                return wallets[0]["Wallets"].id

    @classmethod
    async def add_wallet_summ(cls, user):
        wallet_amount = await Wallet_amountDAO.view_wallet_amount(user.id)
        bot_summ = {}
        for bot in wallet_amount.keys():
            name_coin = wallet_amount[bot]["name"]
            amount_coin = wallet_amount[bot]["amount"]

            summ = 0
            for coin in range(len(name_coin)):
                id_coin = await CoinsDAO.view_coin_id(name_coin[coin])
                price_coin = await CoinExchangeDAO.view_last_cost(id_coin)
                amount = amount_coin[coin]
                summ += price_coin * amount
            bot_summ[bot] = summ
        return bot_summ

    @classmethod
    async def delete_wallet(cls, del_wal: SchemaCheckCoin):
        async with async_session_maker() as session:
            query = select(Wallets).filter(Wallets.owner_id == del_wal.owner_id,
                                           Wallets.coin_id == del_wal.coin_id,
                                           Wallets.bot_id == del_wal.bot_id
                                           )
            result = await session.execute(query)
            wallets = result.mappings().all()
            if not wallets:
                return "Этой монеты нет"
            else:
                wallet_id = wallets[0]["Wallets"].id
                delete_query = delete(Wallet_amount).where(Wallet_amount.wallet_id == wallet_id)
                await session.execute(delete_query)

                delete_wallet_query = delete(Wallets).where(Wallets.id == wallet_id)
                await session.execute(delete_wallet_query)
                await session.commit()
                return "Монета удалена"


class Wallet_amountDAO(BaseDAO):
    model = Wallet_amount

    @classmethod
    async def add_wallet_amount(cls, new_amount: SchemaAddAmount):
        async with async_session_maker() as session:
            add_amount = insert(Wallet_amount).values(
                wallet_id=new_amount.wallet_id,
                tm=func.now(),
                amount=new_amount.amount,
            )
            new_amount = await session.execute(add_amount)
            await session.commit()
            return new_amount

    @classmethod
    async def view_wallet_amount(cls, user_id):
        async with async_session_maker() as session:
            all_bot = set()
            bot_wallet = {}
            all_wallets = await WalletsDAO.find_all(owner_id=user_id)
            for bot_id in range(0, len(all_wallets)):
                all_bot.add(all_wallets[bot_id]["Wallets"].bot_id)
            for bot in all_bot:
                list_of_coins_id = []
                list_of_id = []
                all_coins = []
                all_amount = []
                all_price = []
                all_comparison = []
                for wallet in range(0, len(all_wallets)):
                    if all_wallets[wallet]["Wallets"].bot_id == bot:
                        list_of_coins_id.append(all_wallets[wallet]["Wallets"].coin_id)
                        list_of_id.append(all_wallets[wallet]["Wallets"].id)

                for coin in list_of_coins_id:
                    coins = await CoinsDAO.find_by_id(coin)
                    all_coins.append(coins.name)

                for price in list_of_coins_id:
                    prices = await CoinExchangeDAO.view_last_cost(price)
                    all_price.append(prices)

                for wallet_id in range(0, len(list_of_id)):
                    subquery = (
                        select(Wallet_amount)
                        .where(Wallet_amount.wallet_id == list_of_id[wallet_id])
                        .order_by(Wallet_amount.tm.desc())
                    )
                    result = await session.execute(subquery)
                    row = result.scalar()
                    all_amount.append(row.amount)

                for compare in list_of_coins_id:
                    compares = await CoinExchangeDAO.view_story_exchange(compare)
                    if compares[-1]["value"] > compares[-2]["value"]:
                        all_comparison.append('bigger')
                    else:
                        all_comparison.append('smaller')

                name_price = {
                    "id": list_of_coins_id,
                    "name": all_coins,
                    "amount": all_amount,
                    "price": all_price,
                    "compare": all_comparison
                }
                bot_wallet[bot] = name_price

            return bot_wallet
