from decimal import Decimal
from pydantic import BaseModel


class SchemaAddWallet(BaseModel):
    coin_id: int
    owner_id: int
    bot_id: str


class SchemaAddAmount(BaseModel):
    wallet_id: int
    amount: Decimal


class SchemaCheckCoin(BaseModel):
    coin_id: int
    owner_id: int
    bot_id: str
