from fastapi import APIRouter, Depends, HTTPException, Response, status
from app.coins.dao import CoinExchangeDAO, CoinsDAO
from app.exception import IncorrectEmailOrPasswordException, UserAlreadyExistsException
from app.wallets.dao import Wallet_amountDAO, WalletsDAO
from app.users.dependencies import get_current_admin_user, get_current_user

from app.users.models import Users
from app.wallets.schema import SchemaAddAmount, SchemaAddWallet, SchemaCheckCoin

router = APIRouter(
    prefix="/wallets",
    tags=["Кошельки"],
)


@router.post("/wallet/add")
async def add_new_wallet(
        new_wallet: SchemaAddWallet, current_user: Users = Depends(get_current_admin_user)
):
    return await WalletsDAO.add_wallet(new_wallet)


@router.get("/wallet/last")
async def get_last_wallet_id(current_user: Users = Depends(get_current_admin_user)):
    last_id = await WalletsDAO.view_last_wallet()
    return last_id


@router.post("/wallet/amount/add")
async def add_wallet_amount(
        wallet_amount: SchemaAddAmount,
        current_user: Users = Depends(get_current_admin_user),
):
    try:
        await Wallet_amountDAO.add_wallet_amount(wallet_amount)
        return 1
    except Exception:
        return 0


@router.get("/wallet/view")
async def amount_view(user: Users = Depends(get_current_user)):
    return await Wallet_amountDAO.view_wallet_amount(user.id)


@router.get("/wallet/view/{id}")
async def amount_view_id(id_user: int, user: Users = Depends(get_current_admin_user)):
    data = await Wallet_amountDAO.view_wallet_amount(id_user)

    result = {}
    for key, value in data.items():
        new_dict = {}
        for i in range(len(value["id"])):
            new_dict[value["id"][i]] = value["amount"][i]
        result[key] = new_dict

    return result


@router.post("/wallet/amount/")
async def amount_view_id(wallet: SchemaCheckCoin, user: Users = Depends(get_current_admin_user)):
    wallet_id = await WalletsDAO.view_wallet_amount_id(wallet)
    return wallet_id


@router.delete("/wallet/delete/")
async def delete_wallet_id(wal_del: SchemaCheckCoin, user: Users = Depends(get_current_admin_user)):
    return await WalletsDAO.delete_wallet(wal_del)


@router.post("/wallet/check")
async def check_wallet(check_coin: SchemaCheckCoin, users: Users = Depends(get_current_admin_user)):
    return await WalletsDAO.check_coin(check_coin)


@router.get("/wallets/summ")
async def wallets_summ(user: Users = Depends(get_current_user)):
    return await WalletsDAO.add_wallet_summ(user)
