from datetime import datetime
from app.database import Base
from sqlalchemy.orm import Mapped, mapped_column
import sqlalchemy as sa


class Wallets(Base):
    __tablename__ = "wallets"

    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    owner_id: Mapped[int] = mapped_column(sa.ForeignKey("users.id"))
    coin_id: Mapped[str] = mapped_column(sa.ForeignKey("coins.id"))
    bot_id: Mapped[str] = mapped_column(nullable=True)


class Wallet_amount(Base):
    __tablename__ = "wallet_amount"

    id: Mapped[int] = mapped_column(nullable=False, primary_key=True)
    wallet_id: Mapped[int] = mapped_column(sa.ForeignKey("wallets.id"))
    tm: Mapped[datetime] = mapped_column(nullable=False)
    amount = sa.Column(sa.Numeric(30, 3), nullable=False)
