from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.users.router import router as router_users
from app.coins.router import router as router_coins
from app.wallets.router import router as router_wallets

app = FastAPI(docs_url=None, redoc_url=None)


origins = [
    "http://127.0.0.1:8000",
    "http://192.168.1.148:8080",
    "http://localhost:8080",
    "http://213.171.15.139:8080",
    "http://213.171.15.139",
    "http://finrezcryptobakz.ru",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "OPTIONS", "DELETE", "PATCH", "PUT"],
    allow_headers=["*"],
    expose_headers=["Authorization"],
)


app.include_router(router_users)
app.include_router(router_coins)
app.include_router(router_wallets)
