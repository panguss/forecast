from fastapi import APIRouter, Depends, HTTPException, Response, status
from app.exception import IncorrectEmailOrPasswordException, UserAlreadyExistsException
from app.coins.dao import CoinExchangeDAO, CoinsDAO
from app.users.dependencies import get_current_admin_user, get_current_user
from app.coins.models import Coins
from decimal import Decimal

from app.coins.schema import SchemaAddCoin, SchemaCoinExchange
from app.users.models import Users

router = APIRouter(
    prefix="/coins",
    tags=["Монеты"],
)


@router.post("/coins/add")
async def add_coin(
    new_coin: SchemaAddCoin, current_user: Users = Depends(get_current_admin_user)
):
    return await CoinsDAO.add_coin(new_coin)


@router.post("/coins/exchange/{id}")
async def add_exchange(
    coin_id: int, new_price: Decimal,
    current_user: Users = Depends(get_current_admin_user),
):
    await CoinExchangeDAO.add_coin_exchange(coin_id, new_price)
    return 1


@router.get("/coin/view/{id}")
async def coin_view(
    coin_id: int, current_user: Users = Depends(get_current_admin_user)
):
    name = await CoinsDAO.find_by_id(coin_id)
    price = await CoinExchangeDAO.view_last_cost(coin_id=coin_id)
    name_price = {"name": name.name, "price": price}
    return name_price


@router.get("/coin/id")
async def coin_view_id(
        coin_name: str, current_user: Users = Depends(get_current_admin_user)
):
    coin = await CoinsDAO.view_coin_id(coin_name)
    return coin


@router.get("/coin/all/price")
async def coin_view_price(current_user: Users = Depends(get_current_admin_user)):
    return await CoinsDAO.view_coin_cost()


@router.delete("/coin/delete")
async def coin_delete(coin_id: int, current_user: Users = Depends(get_current_admin_user)):
    await CoinsDAO.delete_coin(coin_id)
    return 1


@router.get("/coin/all")
async def all_coin_view():
    result = await CoinsDAO.get_all_coin()
    return result


@router.get("/coin/view/exchange/{id}")
async def exchange_view(coin_id: int, current_user: Users = Depends(get_current_user)):
    result = await CoinExchangeDAO.view_story_exchange(coin_id=coin_id)
    return result
