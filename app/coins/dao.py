from datetime import datetime
from decimal import Decimal

from sqlalchemy import and_, func, insert, update, select, delete
from app.dao.base import BaseDAO
from app.coins.models import Coin_exchange, Coins
from app.database import async_session_maker
from app.database import engine
from app.coins.schema import SchemaAddCoin, SchemaCoinExchange


class CoinsDAO(BaseDAO):
    model = Coins

    @classmethod
    async def get_all_coin(cls):
        async with async_session_maker() as session:
            result = await CoinsDAO.find_all()
            coin_list = []
            for coin in result:
                coin_list.append(coin["Coins"].name)
            return coin_list



    @classmethod
    async def add_coin(cls, new_coin: SchemaAddCoin):
        async with async_session_maker() as session:
            query = select(Coins).filter_by(name=new_coin.name)
            result = await session.execute(query)
            coin = result.scalar_one_or_none()
            if (coin is None) or (new_coin.name != coin.name):
                add_coin = insert(Coins).values(name=new_coin.name)
                new_coin = await session.execute(add_coin)
                await session.commit()
                return new_coin

    @classmethod
    async def view_coin_id(cls, name):
        async with async_session_maker() as session:
            query = select(Coins).filter_by(name=name)
            result = await session.execute(query)
            coin = result.scalar_one_or_none()
            return coin.id

    @classmethod
    async def view_coin_cost(cls):
        result = await CoinsDAO.find_all()
        coin_list = {}
        name_list = []
        price_list = []
        for coin in result:
            price = await CoinExchangeDAO.view_last_cost(coin["Coins"].id)
            name_list.append(coin["Coins"].name)
            price_list.append(price)
        coin_list["names"] = name_list
        coin_list["prices"] = price_list
        return coin_list

    @classmethod
    async def delete_coin(cls, coin_id: int):
        async with async_session_maker() as session:
            query = select(Coins).where(Coins.id == coin_id)
            result = await session.execute(query)
            coin = result.mappings().all()
            if not coin:
                return "Этой монеты нет"
            else:
                delete_exchange = delete(Coin_exchange).where(Coin_exchange.coin_id == coin_id)
                await session.execute(delete_exchange)
                delete_query = delete(Coins).where(Coins.id == coin_id)
                await session.execute(delete_query)
                await session.commit()


class CoinExchangeDAO(BaseDAO):
    model = Coin_exchange

    @classmethod
    async def add_coin_exchange(cls, coin_id: int, new_price: Decimal,):
        async with async_session_maker() as session:
            add_exchange = insert(Coin_exchange).values(
                coin_id=coin_id,
                tm=func.now(),
                cost=new_price,
            )
            new_exchange = await session.execute(add_exchange)
            await session.commit()
            return new_exchange

    @classmethod
    async def view_last_cost(cls, coin_id: int):
        async with async_session_maker() as session:
            subquery = (
                select(Coin_exchange)
                .where(Coin_exchange.coin_id == coin_id)
                .order_by(Coin_exchange.tm.desc())
            )

            result = await session.execute(subquery)
            row = result.scalar()
            return row.cost

    @classmethod
    async def view_story_exchange(cls, coin_id: int):
        async with async_session_maker() as session:
            time = select(Coin_exchange.tm).where(Coin_exchange.coin_id == coin_id)
            result_time = await session.execute(time)
            rows_time = result_time.scalars().all()
            cost = select(Coin_exchange.cost).where(Coin_exchange.coin_id == coin_id)
            result_cost = await session.execute(cost)
            rows_cost = result_cost.scalars().all()
            data = [{"time": t, "value": v} for t, v in zip(rows_time, rows_cost)]
            return data
