from datetime import datetime
from app.database import Base
from sqlalchemy.orm import Mapped, mapped_column
import sqlalchemy as sa


class Coins(Base):
    __tablename__ = "coins"

    id: Mapped[int] = mapped_column(primary_key=True, nullable=False, unique=True)
    name: Mapped[str] = mapped_column(nullable=False)


class Coin_exchange(Base):
    __tablename__ = "coin_exchange"

    id: Mapped[int] = mapped_column(nullable=False, primary_key=True)
    coin_id: Mapped[int] = mapped_column(sa.ForeignKey("coins.id"))
    tm: Mapped[datetime] = mapped_column(nullable=False)
    cost = sa.Column(sa.Numeric(11, 3), nullable=False)
