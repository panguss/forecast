from decimal import Decimal
from pydantic import BaseModel


class SchemaAddCoin(BaseModel):
    name: str


class SchemaCoinExchange(BaseModel):
    coin_id: int
    new_price: Decimal
