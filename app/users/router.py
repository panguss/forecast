from fastapi import APIRouter, Depends, HTTPException, Response, status
from app.exception import IncorrectEmailOrPasswordException, UserAlreadyExistsException
from app.users.auth import (
    authenticate_user,
    create_acess_token,
    get_password_hash,
)
from app.users.dao import UsersDAO
from app.users.dependencies import get_current_admin_user, get_current_user
from app.users.models import Users

from app.users.schema import SchemaNewRole, SchemaUserLogin, SchemaUserRegister

from typing import List

from app.wallets.dao import WalletsDAO

router = APIRouter(
    prefix="/auth",
    tags=["Auth & Пользователи"],
)


@router.post("/register")
async def register_user(user_data: SchemaUserRegister):
    exiciting_user = await UsersDAO.find_one_or_none(email=user_data.email)
    if exiciting_user:
        raise UserAlreadyExistsException
    hashed_password = get_password_hash(user_data.password)
    await UsersDAO.add(
        email=user_data.email,
        hashed_password=hashed_password,
        agree_terms=user_data.agree_terms,
    )


@router.post("/login")
async def login_user(responce: Response, user_data: SchemaUserLogin):
    user = await authenticate_user(user_data.email, user_data.password)
    if not user:
        raise IncorrectEmailOrPasswordException
    access_token = create_acess_token({"sub": str(user.id)})
    responce.set_cookie("access_token", access_token, httponly=True)
    return access_token


@router.post("/logout")
async def logout_user(response: Response):
    response.delete_cookie("access_token")


@router.get("users/me")
async def read_user_me(current_user: Users = Depends(get_current_user)):
    return current_user


@router.get("/users/all")
async def read_user_all(current_user: Users = Depends(get_current_admin_user)):
    users = await UsersDAO.find_all()
    all_users = []
    for user in range(len(users)):
        all_users.append(users[user]["Users"])
    return all_users


@router.delete("/users/delete/")
async def delete_user(id_user: int, current_user: Users = Depends(get_current_admin_user)):
    delete = await UsersDAO.delete_user_id(id_user)
    if not delete:
        return 0
    else:
        return 1


@router.get("/users/{id}")
async def get_user_id(id: int, current_user: Users = Depends(get_current_admin_user)):
    user = await UsersDAO.find_by_id(id)
    wallet = await WalletsDAO.view_wallet_id(id)
    return user, wallet


@router.patch("/edit_role")
async def edit_role(
    role: SchemaNewRole, current_user: Users = Depends(get_current_admin_user)
):
    await UsersDAO.edit_role(role)
