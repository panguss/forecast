from sqlalchemy import and_, update, delete, select
from app.dao.base import BaseDAO
from app.users.models import Users
from app.database import async_session_maker
from app.database import engine
from app.users.schema import SchemaNewRole
from app.wallets.models import Wallets, Wallet_amount


class UsersDAO(BaseDAO):
    model = Users

    @classmethod
    async def edit_role(cls, role: SchemaNewRole):
        async with async_session_maker() as session:
            puted = (
                update(Users)
                .where(and_(Users.role != role.new_role, Users.id == role.id))
                .values(role=role.new_role)
            )
            puted_user = await session.execute(puted)
            await session.commit()
            return puted_user

    @classmethod
    async def delete_user_id(cls, user_id: int):
        async with async_session_maker() as session:
            wallet_id = []
            select_wallets = select(Wallets.id).where(Wallets.owner_id == user_id)
            wallet_results = await session.execute(select_wallets)
            wallet_records = wallet_results.fetchall()
            for wallet_record in wallet_records:
                wallet_id.append(wallet_record[0])
            if wallet_id:
                delete_wallet_amount = (delete(Wallet_amount)
                                        .where(Wallet_amount.wallet_id.in_(wallet_id))
                                        )
                await session.execute(delete_wallet_amount)
            if wallet_id:
                delete_wallets = (delete(Wallets)
                                  .where(Wallets.id.in_(wallet_id))
                                  )
                await session.execute(delete_wallets)
            deleted_user = (delete(Users)
                            .where(Users.id == user_id)
                            )
            result = await session.execute(deleted_user)
            await session.commit()
            return result
