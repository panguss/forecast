from pydantic import BaseModel, EmailStr


class SchemaUserRegister(BaseModel):
    email: EmailStr
    password: str
    agree_terms: bool


class SchemaUserLogin(BaseModel):
    email: EmailStr
    password: str


class SchemaNewRole(BaseModel):
    id: int
    new_role: str
