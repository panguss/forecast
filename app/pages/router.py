from fastapi import APIRouter, Depends, Request
from fastapi.templating import Jinja2Templates

from app.users.router import read_user_all

router = APIRouter(prefix="/pages", tags=["Фронтенд"])

templates = Jinja2Templates(directory="app/templates")


@router.get("/authorization")
async def get_auth_page(request: Request, all_users: read_user_all):
    return templates.TemplateResponse(
        name="auth.html", context={"request": request, "all_users": all_users}
    )
